
        # The code that defines your stack goes here


response = client.create_trigger(
    Name='string',
    WorkflowName='string',
    Type='SCHEDULED' | 'CONDITIONAL' | 'ON_DEMAND',
    Schedule='string',
    Predicate={
        'Logical': 'AND' | 'ANY',
        'Conditions': [
            {
                'LogicalOperator': 'EQUALS',
                'JobName': 'string',
                'State': 'STARTING' | 'RUNNING' | 'STOPPING' | 'STOPPED' | 'SUCCEEDED' | 'FAILED' | 'TIMEOUT',
                'CrawlerName': 'string',
                'CrawlState': 'RUNNING' | 'CANCELLING' | 'CANCELLED' | 'SUCCEEDED' | 'FAILED'
            },
        ]
    },
    Actions=[
        {
            'JobName': 'string',
            'Arguments': {
                'string': 'string'
            },
            'Timeout': 123,
            'SecurityConfiguration': 'string',
            'NotificationProperty': {
                'NotifyDelayAfter': 123
            },
            'CrawlerName': 'string'
        },
    ],
    Description='string',
    StartOnCreation=True | False,
    Tags={
        'string': 'string'
    }
)
